var prodRequired = { // TWh
	now: 541,
	optimize: 541,
	transition: 433 // -20% en 2030
};

var carbonTax = { // € par t de CO2
	now: 7,
	optimize: 22,
	transition: 22
};

var bounds = {
	cost: 80, // <
	co2: 50, // <
	nuclear: 50, // <= 50 %
	renewable: 40 // >= 40 %
};

/* Sources

 * CRE : Coûts et rentabilité des énergies renouvelables en France métropolitaine, avril 2014
         http://www.cre.fr/documents/publications/rapports-thematiques/couts-et-rentabilite-des-enr-en-france-metropolitaine/consulter-le-rapport
         (éolien, photo-v, biomasse)

 * Cour des comptes :
     1. Le coût de production de l'électricité nucléaire, Actualisation 2014
        http://www.ccomptes.fr/Publications/Publications/Le-cout-de-production-de-l-electricite-nucleaire-actualisation-2014
     2. La politique de développement des énergies renouvelables – juillet 2013 (p. 42, données ADEME)
        http://www.ccomptes.fr/Publications/Publications/La-politique-de-developpement-des-energies-renouvelables

 * RTE :
     1. Bilan électrique 2014
         http://www.rte-france.com/sites/default/files/bilan_electrique_2014.pdf
     2.  Panorama de l'électricité renouvelable 2014
         http://www.rte-france.com/sites/default/files/panorama_des_energies_renouvelables_2014.pdf

 * kd :  Cour des comptes (différent de RTE : <http://www.rte-france.com/fr/eco2mix/eco2mix-co2>)
 * kp :  RTE <http://www.rte-france.com/sites/default/files/bilan_electrique_2014.pdf> p.15
 * co2 : Cour des comptes (différent de RTE, ib p.23) 
 */
var powerSources = [
	{
		name: "eolien-m", // put an icon with this name in assets/images/
		title: "Éolien maritime",
		min: 0,
		max: 30,
		value: 0, // valeur par défaut
		france: 0,
		kd: 0.40,
		kp: 0.4,
		cost: 130, // €/MWh, 130 est le tarif légal, la CdC estime le coût de prod entre 105 et 164
		co2: 7, // kg/MWh
		renewable: 1 // 0 <= rate <= 1
	},
	{
		name: "eolien-t",
		title: "Éolien terrestre",
		min: 0,
		max: 30,
		value: 9,
		france: 9.120,
		kd: 0.226,
		kp: 0.226, // RTE p.18 (tenant compte des installations en cours d'année)
		cost: 82, // €/MWh : 82 est le prix d'achat contractuel, la CRE évalue le coût moyen de prod à 86 (p. 24)
		co2: 7, // kg/MWh
		renewable: 1
	},
	{
		name: "nucleaire",
		title: "Nucléaire",
		min: 0,
		max: 140,
		value: 63,
		france: 63.130,
		kd: 0.85,
		kp: 0.752, // 415.9*1000/(365*24*63.130)
		cost: 60, // Cour des comptes, Le coût de production de l’électricité nucléaire actualisation 2014, p. 163 (contre 49 € en 2010)
		co2: 6,
		renewable: 0
	},
	{
		name: "charbon", // put an icon with this name in assets/images/
		title: "Charbon",
		min: 0,
		max: 30,
		value: 5, // valeur par défaut
		france: 5.119,
		kd: 0.90,
		kp: 0.16, // 8.3*1000/(365*24*6.119)
		cost: 70, // €/MWh
		co2: 1038, // kg/MWh (960 selon RTE)
		renewable: 0
	},
	{
		name: "fioul", // put an icon with this name in assets/images/
		title: "Fioul",
		min: 0,
		max: 30,
		value: 9, // valeur par défaut
		france: 8.883,
		kd: 0.90,
		kp: 0.06, // 4.4*1000/(365*24*8.183)
		cost: 409, // €/MWh
		co2: 704, // kg/MWh (670 selon RTE)
		renewable: 0
	},
	{
		name: "gaz", // put an icon with this name in assets/images/
		title: "Gaz",
		min: 0,
		max: 30,
		value: 10, // valeur par défaut
		france: 10.409,
		kd: 0.90,
		kp: 0.16, // 14.3*1000/(365*24*10.409)
		cost: 100, // €/MWh
		co2: 406, // kg/MWh (460 selon RTE)
		renewable: 0
	},
	{
		name: "hydraulique", // put an icon with this name in assets/images/
		title: "Hydraulique",
		min: 0,
		max: 30,
		value: 25, // valeur par défaut
		france: 25.411,
		kd: 0.87,
		kp: 0.31, // 68.2*1000/(365*24*25.411)
		cost: 20, // €/MWh
		co2: 4, // kg/MWh
		renewable: 1
	},
	{
		name: "photo", // put an icon with this name in assets/images/
		title: "Photovoltaïque",
		min: 0,
		max: 30,
		value: 5, // valeur par défaut
		france: 5.292,
		kd: 0.142,
		kp: 0.142, // installations en cours d'année, donc supérieur à 5.9*1000/(365*24*5.292)
		cost: 229, // €/MWh : 229 est le prix d'achat légal des grandes installations, la CRE évalue le coût moyen des nouvelles installations à 152 € (p.44)
		co2: 55, // kg/MWh
		renewable: 1
	},
	{
		name: "other",
		title: "Autres (déchets…)",
		min: 0,
		max: 30,
		value: 1, // valeur par défaut
		france: 1.579,
		kd: 0.90,
		kp: 0.48, // 6.6*1000/(365*24*1.579)
		cost: 150, // €/MWh - approx
		co2: 50, // kg/MWh - approx
		renewable: 0.77
	},
	{
		name: "decentralized", // put an icon with this name in assets/images/
		title: "Nouveaux acteurs",
		min: 0,
		max: 30,
		value: 0, // valeur par défaut
		france: 0,
		kd: 1,
		kp: 0.48, // idem "Autres"
		cost: 150, // €/MWh - idem "Autres"
		co2: 50, // kg/MWh - idem "Autres"
		renewable: 0.77 // idem "Autres"
	}
];


