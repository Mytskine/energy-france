var state = 'now'; // now | optimize | transition

Chart.defaults.global.animation = !1;

var sourcesByName;
function getSource(name) {
	if (typeof sourcesByName === 'undefined') {
		sourcesByName = {};
		for (var i=0 ; i < powerSources.length ; i++) {
			sourcesByName[powerSources[i].name] = powerSources[i];
		}
	}
	return sourcesByName[name];
}

function getUserInput(name) {
	return parseInt($('input.energy[name="' + name + '"]').first().val());
}

function updateResults() {
	var c = {
		power: 0,
		production: 0,
		productionReal: 0,
		productionRealRenewable: 0,
		cost: 0,
		co2: 0,
		nuclearShare: 0,
		renewableShare: 0
	};
	$.each(powerSources, function(i) {
		var source = powerSources[i];
		var sourcePower = getUserInput(source.name);
		c.power += sourcePower; // MWh
		source.production = sourcePower * source.kd * 365 * 24;
		source.productionReal = sourcePower * source.kp * 365 * 24;
		c.production += source.production; // production annuelle
		c.productionReal += source.productionReal;
		if (source.renewable) {
			c.productionRealRenewable += source.productionReal;
		}
		c.cost += source.productionReal * source.cost; // MWh réels * taux
		c.co2  += source.productionReal * source.co2; // production réelle * taux
	});
	c.co2 /= c.productionReal;
	c.cost /= c.productionReal;
	c.cost += c.co2 * parseInt($("#carbon-tax").val()) / 1000;
	c.power = Math.round(c.power);

	c.nuclearShare = Math.round(
		100 * getUserInput('nucleaire') * getSource('nucleaire').kp * 365 * 24 / c.productionReal
	);
	c.renewableShare = Math.round(
		100 * c.productionRealRenewable / c.productionReal
	);

	$("#results .power .result").text(c.power);
	$("#results .production .result").text(Math.round(c.production/1000));
	$("#results .production-real .result").text(Math.round(c.productionReal/1000));
	$("#results .cost .result").text(Math.round(c.cost));
	$("#results .co2 .result").text(formatNum(c.co2, 2));

	$("#results .nuclear-share-power .result").text(
		Math.round(100 * getUserInput('nucleaire') / c.power)
	);
	$("#results .nuclear-share-production .result").text(
		Math.round(100 * getUserInput('nucleaire') * getSource('nucleaire').kd * 365 * 24 / c.production)
	);
	$("#results .nuclear-share-real .result").text(c.nuclearShare);
	$("#results .renewable-share-real .result").text(c.renewableShare);

	updateObjectives(state, c);
	updateSharesChart();
}

function updateObjectives(state, computations) {
	var prodDiff = Math.round((computations.productionReal - 1000 * prodRequired[state]) / 1000);
	if (prodDiff >= 20) {
		$("#results").attr('class', "");
		$("#results .production-real .comment").html(prodDiff + " TWh en excès")
			.addClass("alert");
	} else if (prodDiff <= -1) {
		$("#results").attr('class', "blackout");
		$("#results .production-real .comment").html((-1 * prodDiff) + " Twh manquant")
			.addClass("alert");
	} else {
		$("#results").attr('class', "perfect");
		$("#results .production-real .comment").html("").removeClass("alert");
	}

	if (state === 'now') {
		$("#results .cost .comment").removeClass("alert");
		$("#results .co2 .comment").removeClass("alert");
		$("#results .nuclear-share-real .comment").removeClass("alert");
		$("#results .renewable-share-real .comment").removeClass("alert");
		$("#field-decentralized").addClass('hidden');
	}

	if (state === 'optimize' || state === 'transition') {
		if (Math.round(computations.cost) > bounds.cost) {
			$("#results .cost .comment").addClass("alert");
		} else {
			$("#results .cost .comment").removeClass("alert");
		}
		if (computations.co2 > bounds.co2) {
			$("#results .co2 .comment").addClass("alert");
		} else {
			$("#results .co2 .comment").removeClass("alert");
		}
		$("#field-decentralized").addClass('hidden');
	}
	if (state === 'transition') {
		if (computations.nuclearShare > bounds.nuclear) {
			$("#results .nuclear-share-real .comment").addClass("alert");
		} else {
			$("#results .nuclear-share-real .comment").removeClass("alert");
		}
		if (computations.renewableShare < bounds.renewable) {
			$("#results .renewable-share-real .comment").addClass("alert");
		} else {
			$("#results .renewable-share-real .comment").removeClass("alert");
		}
		$("#field-decentralized").removeClass('hidden');
	}
}

var sharesChartPower;
var sharesChartProd;
function updateSharesChart() {
	var colors = ["#5DA5DA", "#FAA43A", "#60BD68", "#F17CB0", "#B2912F", "#B276B2", "#6666BD", "#DECF3F", "#4D4D4D", "#F15854"];
	var dataPower = [];
	var dataProd = [];
	for (var i=0 ; i < powerSources.length ; i++) {
		var source = powerSources[i];
		if (source.name !== "decentralized" || state === "transition") {
			dataPower.push({
				value: getUserInput(source.name),
				color: colors[i],
				label: source.title
			});
			dataProd.push({
				value: Math.round(source.productionReal / 1000), // GWh
				color: colors[i],
				label: source.title
			});
		}
	}
	$("canvas *").remove();
	var ctx = document.getElementById("chart-shares-power").getContext("2d");
	if (typeof sharesChartPower !== 'undefined') {
		sharesChartPower.destroy();
		$(".doughnut-legend").remove();
	}
	sharesChartPower = new Chart(ctx).Doughnut(dataPower);

	$("#chart-shares-power").parent().prepend(sharesChartPower.generateLegend());

	var ctx2 = document.getElementById("chart-shares-prod").getContext("2d");
	if (typeof sharesChartProd !== 'undefined') {
		sharesChartProd.destroy();
	}
	sharesChartProd = new Chart(ctx2).Doughnut(dataProd);
}

function onStateChange(event) {
	state = $(event.target).attr("data-value");

	$("#results .prod-required .result").text(prodRequired[state]);
	$("#carbon-tax").val(carbonTax[state]);

	$("#status p").html($(event.target).attr("title"));
	$("#status button").removeClass("active");
	$(event.target).addClass("active");
	if (state === 'now') {
		for (var i=0 ; i < powerSources.length ; i++) {
			var source = powerSources[i];
			$('input.energy[name="' + source.name + '"]').first().val(Math.round(source.france));
		}
		updateSliders();
		$("#results .cost .comment").text("");
		$("#results .co2 .comment").text(" ");
		$("#results .nuclear-share-real .comment").text("");
		$("#results .renewable-share-real .comment").text("");
		$("#transition-image").hide();
	} else if (state === 'optimize' || state === 'transition') {
		$("#results .cost .comment").text("objectif ≤ " + bounds.cost + " €/MWh");
		$("#results .co2 .comment").text("objectif ≤ " + bounds.co2 + " kg/MWh");
	}
	if (state === 'transition') {
		$("#results .nuclear-share-real .comment").text("objectif ≤ " + bounds.nuclear + " %");
		$("#results .renewable-share-real .comment").text("objectif ≥ " + bounds.renewable + " %");
		$("#transition-image").show();
	}
	updateResults();
}

function updateSliders() {
	$(".slider-vertical").each(function() {
		source = $(this).parent().data("source");
		$(this).slider({
			orientation: "vertical",
			range: "min",
			min: source.min,
			max: source.max,
			value: source.value,
			slide: function(event, ui) {
				$(event.target).closest(".input-field").find("input.energy").val(ui.value);
				updateResults();
			}
		});
	});
	$("#slider-carbon").slider({
		orientation: "horizontal",
		range: "min",
		min: 0,
		max: 200,
		value: $("#carbon-tax").val(),
		slide: function(event, ui) {
			$("#carbon-tax").val(ui.value);
			updateResults();
		}
	});
}

function formatNum(num, decimals) {
	var rounded;
	if (decimals > 0) {
		rounded = Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
	} else {
		rounded = Math.round(num);
	}
	return rounded.toLocaleString('fr-FR');
}

$(function() {
	var container = $("#sources");
	var tableBodyNode = $("table.parametres > tbody");

	for (var i=0 ; i < powerSources.length ; i++) {
		var source = powerSources[i];
		var node = $('<div class="input-field" id="field-' + source.name + '">' +
			'<img src="assets/images/' + source.name + '.png" />' +
			'<div>' +
			'<label for="' + source.name + '">' +
			source.title + ' :</label> ' +
			'<input type="text" class="energy" name="' + source.name + '" value="' + source.value + '" /> GWe '+
			'<div class="france"><span>France 2014 :</span> ' + formatNum(source.france, 2) + ' GWe</div>' +
			'<div class="france" title="Coefficient de disponibilité"><span>Kd :</span> ' + formatNum(source.kd, 2) + '</div>' +
			'<div class="france" title="Coefficient de production (disponibilité &times; utilisation), aussi appelé facteur de charge"><span>Kp :</span> ' + formatNum(source.kp, 2) + '</div>' +
			'</div>' +
			'<div class="slider-vertical"></div>' +
			'</div>'
		);
		node.data("source", source);
		container.append(node);

		tableBodyNode.append(
			$('<tr>' +
				'<td>' + source.title + '</td>' +
				'<td>' + formatNum(source.france, 3) + ' MW</td>' +
				'<td>' + formatNum(source.kd * 100, 0) + ' %</td>' +
				'<td>' + formatNum(source.kp * 100, 0) + '%</td>' +
				'<td>' + source.cost + ' €/MWh</td>' +
				'<td>' + source.co2 + ' kg/MWh</td>' +
				'</tr>'
			)
		);
	}
	updateResults();
	updateSliders();

	$("table.parametres").removeClass("hidden");

	container.on("keyup", "input.energy", function(event) {
		var value = $(this).val();
		$(this).closest(".input-field").find(".slider-vertical").slider("value", value);
		updateResults();
	});
	$("#carbon-tax").on("keyup", function(event) {
		$("#slider-carbon").slider("value", $("#carbon-tax").val());
		updateResults();
	});

	$("#status").on("click", "button", onStateChange);
	$("#status").find('button[data-value="now"]').click();

	$("#tabs").tabs({
		activate: function(event, ui) {
			var scrollTop = $(window).scrollTop();
			window.location.hash = ui.newPanel.attr('id');
			$(window).scrollTop(scrollTop);
		}
	});
});
